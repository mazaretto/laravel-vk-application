@include('templates.head')
    <div id="app" class="container profile">
        @include('profile.info')
        @include('profile.friends')
    </div>
@include('templates.footer')