<section class="profile__block shadow shadow-lg bg-light p-5">
    <div class="row">
        <div class="col-xs-3">
            <div class="profile__block-image">
                <img src="{{ $user['avatar'] }}" class="rounded-circle bg-white border-dark shadow" alt="{{ $user['first_name'] }}" />
            </div>
        </div>
        <div class="col">
            <div class="profile__block-title">
                <h1 id="{{ $user['user_id'] }}">
                    <a target="_blank" href="https://vk.com/id{{ $user['user_id']  }}">
                        {{$user['first_name'] . " " . $user['last_name']}}
                    </a>
                </h1>
            </div>
            <div class="profile__block-info">
                <p>Ваш ID: <b>{{ $user['user_id'] }}</b></p>
            </div>
            <div class="profile__block-buttons">
                <a href="{{ $exitURI }}" class="btn btn-danger">Выход</a>
            </div>
        </div>
    </div>
</section>