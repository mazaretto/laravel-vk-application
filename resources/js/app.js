
import LoadMore from './components/LoadMore'
import Friends from './components/Friends'
import { Plugin } from 'vue-fragment'

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// set components
Vue.component('load-more', LoadMore)
Vue.component('friends-list', Friends)

// using plugins
Vue.use(Plugin)

// functions snippets
const updateOnlineUsersButton = () => {
    const countOnlineUsers = document.querySelectorAll('.online').length
    friends_online.innerHTML = 'Друзей онлайн: ' + countOnlineUsers
}

new Vue({
    el: '#app',

    data: {
        friendsList: [],

        buttonText: 'plus.svg',
        isSort: false
    },

    methods: {
        loadMoreFriends () {
            let allFrields = document.querySelectorAll('.profile__friends-item').length
            this.buttonText = 'loader.gif'
            fetch('/friends/' + allFrields * 2)
                .then(data => data.json())
                .then(async response => {
                    const { friends } = response
                    this.friendsList = this.friendsList.concat(friends.items)
                    this.buttonText = 'plus.svg'

                    updateOnlineUsersButton()
                })
        },

        sortByOnline () {

            this.isSort = !this.isSort

            const friendsOnline = document.querySelectorAll('.online'),
                  friendsContainer = document.querySelector('.friends_row');

            /**
             * Copy friends in array and get root element
             * @type {(Node & ParentNode)[]}
             */
            const copyFriends = Object.assign([], friendsOnline).map(item => {
                return item.parentNode.parentNode.parentNode
            })

            /**
             * Remove old friends
             */
            friendsOnline.forEach(item => item.parentNode.parentNode.parentNode.remove())

            /**
             * Append sorting friends
             */
            if(this.isSort) {
                copyFriends.forEach(item => friendsContainer.prepend(item))
            } else {
                copyFriends.forEach(item => friendsContainer.append(item))
            }
        }
    },

    mounted () {
        updateOnlineUsersButton()
    }
});
