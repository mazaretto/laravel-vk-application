<?php

/**
 * Main route
 */
Route::get('/', 'AppController@index');

Route::get('/exit', 'AppController@userExit');

/**
 * Get all friends
 */
Route::get('/friends/{offset}', 'AppController@loadFriends');