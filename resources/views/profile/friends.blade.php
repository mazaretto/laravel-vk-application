<section class="profile__friends shadow shadow-lg bg-light p-3">
    <div class="col">
        <h3>Ваши друзья {{ $user['friends']->count }}</h3>
        <div class="row">
            <div class="col">
                <button type="button" class="btn btn-info" id="friends_online" @click="sortByOnline">Друзья онлайн: 0</button>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="row friends_row">
            @foreach($user['friends']->items as $friend)
                <div class="shadow-none p-1 col-md-4 mb-2 bg-white rounded profile__friends-item">
                    <div class="row{{ ($friend->is_closed === 1) ? ' close_profile' : '' }}" data-userid="{{ $friend->id }}" data-user-online="{{ $friend->online }}">
                        <div class="col-xs-4">
                            <img src="{{ $friend->photo_100 }}" class="rounded-circle" alt="">
                        </div>
                        <div class="col">
                            <p>
                                <a href="https://vk.com/id{{ $friend->id }}" target="_blank">
                                    {{ $friend->first_name . " " . $friend->last_name }}
                                </a>
                            </p>
                            @if($friend->online)
                                <p class="online">Online</p>
                            @else
                                <p class="offline">Offline</p>
                            @endif
                            <p>
                                <button type="button" class="btn btn-success">Написать</button>
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
            <!-- vue template, he loading more friends -->
            <friends-list :items="friendsList" />
        </div>
        <div class="col text-center">
            <!-- button load more -->
            <load-more :text="buttonText" :onload="loadMoreFriends" />
        </div>
    </div>
</section>