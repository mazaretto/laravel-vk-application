<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AppController extends Controller
{
    private $client_id = "6835918";
    private $client_secret = "qlJrDsqvFiXt3fgNShIl";
    private $redirect_uri = "http://app.loc/";
    private $access_token = "";
    private $user_id = "";
    private const VK_AUTH_URI = "https://oauth.vk.com/";
    private const VK_API_URI = "https://api.vk.com/method/";
    private const VK_API_VERSION = "5.92";
    private $user = null;
    private const AUTH_COOKIE = 'VK_USER';

    public function index () {

        $authCook = self::AUTH_COOKIE;
        if (isset($_GET['code'])) {
            $data = $this->getAccessToken($_GET['code']);
            $getSession = session($authCook);

            if (isset($getSession)) {
                return back();
            } else {
                session([
                    $authCook => $data->access_token,
                    'user_id' => $data->user_id
                ]);

                return redirect('../');
            }
        } else {
            $isUserLoggedIn = session($authCook);
            if(isset($isUserLoggedIn)) {
                $this->access_token = trim(session($authCook));

                $this->user_id = trim(session('user_id'));

                return $this->getUserProfile();
            } else {
                return view('index', [
                    'pageName' => 'Главная',
                    'authURI' => $this->getAuthURI()
                ]);
            }
        }
    }

    private function getUserFriends ($offset) {
        $requestURI = self::VK_API_URI . "friends.get?user_id=" . $this->user_id . "&v=" . self::VK_API_VERSION . "&fields=photo_100&count=10&offset=".$offset."&access_token=" . $this->access_token;

        $client = new Client();
        $res = $client->request('GET', $requestURI)->getBody();
        $data = json_decode($res)->response;

        return $data;
    }

    public function loadFriends ($offset) {
        $this->access_token = session(self::AUTH_COOKIE);

        $friends = $this->getUserFriends($offset);

        return json_encode([ 'friends' => $friends ]);
    }

    private function getUserProfile () {
        $client = new Client();
        $requestURI = self::VK_API_URI . "users.get?user_id=". $this->user_id . "&v=" . self::VK_API_VERSION . "&fields=first_name,last_name,photo_big&access_token=" . $this->access_token;
        $res = $client->request('GET', $requestURI)->getBody();
        $data = json_decode($res)->response[0];

        $this->user = $data;

        $friends = $this->getUserFriends(10);

        return view('profile.index', [
            'pageName' => 'Профиль | ' . $data->first_name,
            'pageIcon' => $data->photo_big,
            'exitURI' => '/exit',
            'user' => [
                'user_id' => $this->user_id,
                'first_name' => $data->first_name,
                'last_name' => $data->last_name,
                'avatar' => $data->photo_big,

                'friends' => $friends
            ]
        ]);
    }

    public function userExit (Request $request) {
        if( $request->session()->remove(self::AUTH_COOKIE) ) {
            return redirect('../');
        }
    }

    private function getAccessToken ($code) {
        try {
            $client = new Client();

            $res = $client->request('GET', $this->getAccessTokenURI($code));
            $data = json_decode($res->getBody());

            if ($data->access_token) {
                return $data;
            }

        } catch(\Exception $e) {}
    }

    private function getAccessTokenURI ($code) {
        return self::VK_AUTH_URI . "access_token?v=" . self::VK_API_VERSION . "&code=". $code ."&client_id=" . $this->client_id . "&client_secret=" . $this->client_secret . "&redirect_uri=" . $this->redirect_uri;
    }

    private function getAuthURI () {
        return self::VK_AUTH_URI . "authorize?client_id=" . $this->client_id . "&response_type=code&redirect_uri=". $this->redirect_uri ."&v=" . self::VK_API_VERSION;
    }
}
